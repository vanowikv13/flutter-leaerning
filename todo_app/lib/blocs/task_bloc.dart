import 'dart:async';

import 'package:rxdart/subjects.dart';
import 'package:todo_app/blocs/bloc_base.dart';
import 'package:todo_app/models/task.dart';
import 'package:todo_app/assets/enum_options.dart';

class TaskBloc extends Bloc {
  TaskData _task;

  final Sink<Map<OperationType, TaskData>> operation;

  BehaviorSubject<TaskData> taskController = BehaviorSubject<TaskData>();
  Sink<TaskData> get taskOperation => taskController.sink;
  Stream<TaskData> get task => taskController.stream;

  TaskBloc(this.operation, this._task) {
    taskOperation.add(_task);
  }

  void deleteTask() {
    operation.add({OperationType.deleting: _task});
  }

  void updateTaskIsDoneStatus(bool value) {
    _task.isDone = value;
    operation.add({OperationType.updatingList: _task});
  }

  @override
  void dispose() {
    taskController.close();
  }
}
