import 'dart:async';

import 'package:todo_app/assets/enum_options.dart';
import 'package:todo_app/blocs/task_bloc.dart';
import 'package:todo_app/models/task.dart';
import 'package:todo_app/blocs/bloc_base.dart';

class TaskOperationBloc implements Bloc {
  TaskData _copy;
  bool _isDisable = false;

  Sink<Map<OperationType, TaskData>> _toDoListOperation;

  Sink<TaskData> _taskOperation;

  StreamController<TaskData> _dataFlowController = StreamController<TaskData>();
  Stream<TaskData> get taskData => _dataFlowController.stream;

  StreamController<bool> _isButtonDisable = StreamController<bool>();
  Stream<bool> get isDisable => _isButtonDisable.stream;

  TaskOperationBloc(TaskBloc _bloc,TaskData _task) {
    _dataFlowController.sink.add(_task);
    _copyObject(_task);
    _toDoListOperation = _bloc.operation;
    _taskOperation = _bloc.taskOperation;
  }

  void _copyObject(data) {
    final id = data.id;
    final mapTask = Map.from(data.getMapFromTaskData());
    _copy = TaskData.setFromMap(id, mapTask[id]);
    _dataFlowController.sink.add(_copy);
    updateButton();
  }

  void updateTaskFromTaskData() {
    _taskOperation.add(_copy);
    _toDoListOperation.add({OperationType.updateSingle: _copy});
  }

  void updateTitle(String title) {
    _copy.title = title;
    updateButton();
  }

  void updateDate(DateTime date) {
    _copy.date = date;
    updateButton();
  }

  void updateButton() {
    _isDisable = _copy.title.length > 0 && _copy.date != null ? false : true;
    _isButtonDisable.sink.add(_isDisable);
  }

  @override
  void dispose() {
    _dataFlowController.close();
    _isButtonDisable.close();
  }
}
