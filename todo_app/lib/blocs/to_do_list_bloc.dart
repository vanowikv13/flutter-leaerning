import 'dart:async';

import 'package:rxdart/subjects.dart';
import 'package:todo_app/blocs/bloc_base.dart';
import 'package:todo_app/models/task.dart';
import 'package:todo_app/services/database.dart';
import 'package:todo_app/assets/enum_options.dart';

class ToDoListBloc extends Bloc {
  List<TaskData> tasks = [];
  DatabaseService _dbService;

  final _operationController = BehaviorSubject<Map<OperationType, TaskData>>();
  Sink<Map<OperationType, TaskData>> get operation => _operationController.sink;

  final _taskListsController = BehaviorSubject<List<TaskData>>();
  Stream<List<TaskData>> get taskList => _taskListsController.stream;

  ToDoListBloc(_uid) {
    _dbService = DatabaseService(uid: _uid);
    _getTaskList();
    _operationController.stream.listen(handleOperation);
  }

  void _getTaskList() async {
    _dbService.getTaskList().then((tasks) {
      if (tasks != null) {
        this.tasks = tasks;
        _taskListsController.sink.add(this.tasks);
      }
    });
  }

  void handleOperation(Map<OperationType, TaskData> operation) {
    operation.forEach((k, v) {
      switch (k) {
        case OperationType.updatingList:
          final task = tasks.firstWhere((item) => v.id == item.id);
          tasks[tasks.indexOf(task)] = v;
          _dbService.updateData(v);
          _taskListsController.sink.add(tasks);
          break;
        case OperationType.updateSingle:
          final task = tasks.firstWhere((item) => v.id == item.id);
          tasks[tasks.indexOf(task)] = v;
          _dbService.updateData(v);
          break;
        case OperationType.adding:
          tasks.add(v);
          _dbService.addTask(v);
          _taskListsController.sink.add(tasks);
          break;
        case OperationType.deleting:
          _dbService.deleteTask(v.id);
          tasks.remove(v);
          _taskListsController.sink.add(tasks);
          break;
      }
    });
  }

  @override
  void dispose() {
    _taskListsController.close();
    _operationController.close();
  }
}
