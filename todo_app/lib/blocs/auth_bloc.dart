import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_app/assets/enum_options.dart';
import 'package:todo_app/blocs/bloc_base.dart';

class AuthBloc implements Bloc {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  AuthOption _option = AuthOption.signIn;
  String _email;
  String _password;

  StreamController<AuthOption> _authOptionController =
      StreamController<AuthOption>();
  Stream<AuthOption> get authOperation => _authOptionController.stream;

  StreamController<Map<AuthMessage, String>> _message =
      StreamController<Map<AuthMessage, String>>.broadcast();
  Stream<Map<AuthMessage, String>> get message => _message.stream;

  AuthBloc() {
    _authOptionController.sink.add(_option);
  }

  void setLogin(String login) {
    _email = login;
  }

  void setPassword(String password) {
    _password = password;
  }

  void changeAuthOperation() {
    if (_option == AuthOption.signIn)
      _option = AuthOption.signUp;
    else
      _option = AuthOption.signIn;
    _authOptionController.sink.add(_option);
  }

  void signIn() async {
    try {
      var user = await _handleSignInEmail(_email, _password);
      _message.sink.add({AuthMessage.uid: user.uid});
    } catch (error) {
      _message.sink.add({AuthMessage.error: error.message});
    }
  }

  void signUp() {
    _handleSignUp(_email, _password).then((user) {
      changeAuthOperation();
    });
  }

  Future<FirebaseUser> _handleSignInEmail(String email, String password) async {
    AuthResult result =
        await _auth.signInWithEmailAndPassword(email: email, password: password);
    final FirebaseUser user = result.user;

    assert(user != null);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    print('signInEmail succeeded: $user');
    return user;
  }

  Future<FirebaseUser> _handleSignUp(email, password) async {
    AuthResult result = await _auth.createUserWithEmailAndPassword(
        email: email, password: password);
    final FirebaseUser user = result.user;

    assert(user != null);
    assert(await user.getIdToken() != null);

    return user;
  }

  @override
  void dispose() {}
}
