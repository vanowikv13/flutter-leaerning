import 'package:flutter/material.dart';
import 'package:todo_app/blocs/bloc_base.dart';

class Provider<T extends Bloc> extends StatefulWidget {
  Provider({
    Key key,
    @required this.child,
    @required this.bloc,
  }): super(key: key);

  final T bloc;
  final Widget child;

  @override
  _ProviderState<T> createState() => _ProviderState<T>();

  static T of<T extends Bloc>(BuildContext context){
    Provider<T> provider = context.findAncestorWidgetOfExactType<Provider<T>>();
    return provider.bloc;
  }
}

class _ProviderState<T> extends State<Provider<Bloc>>{
  @override
  void dispose(){
    widget.bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    return widget.child;
  }
}

