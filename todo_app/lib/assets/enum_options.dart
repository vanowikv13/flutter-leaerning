enum OperationType { adding, deleting, updatingList, updateSingle }

enum AuthOption { signIn, signUp }

enum AuthMessage { uid, error }
