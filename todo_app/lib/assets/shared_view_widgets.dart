import 'package:flutter/material.dart';
import 'package:todo_app/assets/shared_styles.dart';

class SharedViewWidget {
  static appBar({title, iconTheme = false, actions = const <Widget>[]}) {
    return AppBar(
      title: Text(
        title,
        style: SharedStyles.titleText,
      ),
      centerTitle: true,
      iconTheme: iconTheme ? IconThemeData(color: SharedStyles.returnArrowFromNewView) : null,
      automaticallyImplyLeading: iconTheme,
      backgroundColor: SharedStyles.titleTextBackGroundColor,
      actions: actions,
    );
  }
}
