import 'package:flutter/material.dart';

class SharedStyles {
  //colors
  static const Color titleTextBackGroundColor = Colors.white;

  static const Color titleColorText = Colors.black;

  static const Color returnArrowFromNewView = Colors.black;

  static const Color authInputIconColor = Colors.black;

  static const themeColors = {
    'primaryColor': Colors.red,
    'secondaryColor': Colors.black,
    'thirdColor': Colors.white,
  };

  //text styles
  static const TextStyle titleText =
      TextStyle(fontSize: 22.0, color: titleColorText);

  static const TextStyle mediumText = TextStyle(fontSize: 18.0);

  static const TextStyle mediumTextLineThrough =
      TextStyle(fontSize: 18.0, decoration: TextDecoration.lineThrough);

  static const TextStyle smallText = TextStyle(fontSize: 12.0);

  static const TextStyle expansionTileTextTitleStyle = TextStyle(fontSize: 18.0, color: Colors.red);

  static const TextStyle authInputLabelTextStyle =
      TextStyle(color: Colors.black);

  static const TextStyle errorMessage = TextStyle(
    fontSize: 13.0,
    color: Colors.red,
    height: 1.0,
  );

  static const TextStyle textAuthInput = TextStyle(color: Colors.black);

  //border fields style
  static const BorderRadius borderRadiusForInput =
      BorderRadius.all(Radius.circular(10.0));

  static const OutlineInputBorder authInputBorder = OutlineInputBorder(
    borderRadius: const BorderRadius.all(
      const Radius.circular(8.0),
    ),
  );
}
