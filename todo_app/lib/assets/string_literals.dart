class StringLiterals {
  static const ADD_TASK_VIEW_TITLE                                = "Adding new task";
  static const ADD_TASK_VIEW_TITLE_HINT_TEXT                      = "Adding new task";
  static const ADD_TASK_VIEW_BUTTON_SAVE_TEXT                     = "Save task";

  static const TO_DO_LIST_VIEW_DONE_LIST_TITLE                    = "Done";

  static const MAIN_TITLE                                         = "To Do List";

  static const TASK_DETAILS_VIEW_TITLE                            = "Task Details";

  static const SIGN_UP_TITLE                                      = "Sign up";
  static const SIGN_IN_TITLE                                      = "Sign in";
  static const ERROR_MESSAGE_IN_LOGIN_WHEN_FIREBASE_DOES_NOT_WORK ="Error: Wrong email or password";
  static const EMAIL_LABEL                                        = "Email Address";
  static const PASSWORD_LABEL                                     = "Password";
  static const VALIDATOR_EMPTY_PASSWORD_MESSAGE                   = "Password can\'t be empty";
  static const VALIDATOR_EMPTY_EMAIL_MESSAGE                      = "Email can\'t be empty";
  static const VALIDATOR_BAD_PASSWORD                             = "Password is not secure";
  static const VALIDATOR_BAD_EMAIL                                = "Email is not correct";
}