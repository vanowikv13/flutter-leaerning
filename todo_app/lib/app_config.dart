import 'package:intl/intl.dart';

class AppConfig {
  static final DateFormat format = DateFormat("yyyy-MM-dd hh-mm");

  static const String taskDatabaseName = 'tasks';
}