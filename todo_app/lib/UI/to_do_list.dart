import 'package:flutter/material.dart';
import 'package:todo_app/assets/shared_styles.dart';
import 'package:todo_app/models/task.dart';
import 'package:todo_app/UI/task.dart';
import 'package:todo_app/blocs/to_do_list_bloc.dart';
import 'package:todo_app/blocs/bloc_provider.dart';
import 'package:todo_app/blocs/task_bloc.dart';
import 'package:todo_app/UI/add_task.dart';
import 'package:todo_app/assets/string_literals.dart';
import 'package:todo_app/assets/shared_view_widgets.dart';

class ListPage extends StatelessWidget {
  List<Widget> _buildList(List<TaskData> tasks, operation) {
    List<Widget> toDo = [], done = [];
    tasks.forEach((f) {
      if (f.isDone)
        done.add(Provider(
          bloc: TaskBloc(operation, f),
          child: Task(),
        ));
      else
        toDo.add(Provider(
          bloc: TaskBloc(operation, f),
          child: Task(),
        ));
    });
    return toDo +
        (done.length > 0
            ? [
                Container(
                    margin: EdgeInsets.only(top: 12.0),
                    child: Theme(
                      child: ExpansionTile(
                        children: done,
                        title: Text(
                          StringLiterals.TO_DO_LIST_VIEW_DONE_LIST_TITLE,
                          style: SharedStyles.mediumText,
                        ),
                      ),
                      data: ThemeData(
                        accentColor: Colors.red,
                    ))),
              ]
            : []);
  }

  @override
  Widget build(BuildContext context) {
    final ToDoListBloc bloc = Provider.of<ToDoListBloc>(context);
    return Scaffold(
      appBar: SharedViewWidget.appBar(
        title: StringLiterals.MAIN_TITLE,
        iconTheme: false,
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        backgroundColor: SharedStyles.themeColors['primaryColor'],
        onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddTask(),
            )),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            )
          ],
        ),
      ),
      body: Container(
        margin: const EdgeInsets.only(bottom: 10.0, top: 10.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: StreamBuilder(
                initialData: List<TaskData>(),
                stream: bloc.taskList,
                builder: (BuildContext context,
                    AsyncSnapshot<List<TaskData>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView(
                        children: _buildList(snapshot.data, bloc.operation));
                  }
                  return SizedBox.shrink();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
