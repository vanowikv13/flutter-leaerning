import 'package:flutter/material.dart';
import 'package:todo_app/blocs/task_bloc.dart';
import 'package:todo_app/blocs/bloc_provider.dart';
import 'package:todo_app/blocs/task_operation_bloc.dart';
import 'package:todo_app/models/task.dart';
import 'package:todo_app/services/date_time_operation.dart';
import 'package:todo_app/assets/shared_styles.dart';
import 'package:todo_app/UI/task_details.dart';

class Task extends StatefulWidget {
  @override
  _TaskState createState() => _TaskState();
}

class _TaskState extends State<Task> {
  TextStyle setTaskTitleStyle({isDone = false}) {
    return isDone
        ? SharedStyles.mediumTextLineThrough
        : SharedStyles.mediumText;
  }

  @override
  Widget build(BuildContext context) {
    TaskBloc bloc = Provider.of<TaskBloc>(context);
    return StreamBuilder(
        stream: bloc.task,
        builder: (BuildContext context, AsyncSnapshot<TaskData> snapshot) {
          if (snapshot.hasData) {
            return GestureDetector(
                child: Card(
                  child: ListTile(
                    leading: Checkbox(
                      value: snapshot.data.isDone ?? false,
                      onChanged: bloc.updateTaskIsDoneStatus,
                    ),
                    title: Text('${snapshot.data.title}',
                        style: setTaskTitleStyle(isDone: snapshot.data.isDone)),
                    subtitle: Text(
                      '${DataTimeOperation.getFormatedData(snapshot.data.date)}',
                      style: SharedStyles.smallText,
                    ),
                    trailing: IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: SharedStyles.themeColors['primaryColor'],
                      ),
                      onPressed: () => bloc.deleteTask(),
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Provider<TaskOperationBloc>(
                        child: TaskDetails(),
                        bloc: TaskOperationBloc(bloc, snapshot.data),
                      ),
                    ),
                  );
                });
          } else {
            return Card(
              child: SizedBox.shrink(),
            );
          }
        });
  }
}
