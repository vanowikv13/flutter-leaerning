import 'package:flutter/material.dart';
import 'package:todo_app/models/task.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:todo_app/blocs/bloc_provider.dart';
import 'package:todo_app/services/date_time_operation.dart';
import 'package:todo_app/assets/string_literals.dart';
import 'package:todo_app/assets/shared_styles.dart';
import 'package:todo_app/app_config.dart';
import 'package:todo_app/blocs/task_operation_bloc.dart';
import 'package:todo_app/assets/shared_view_widgets.dart';

class TaskDetails extends StatelessWidget {
  void _submitTask(BuildContext context, TaskOperationBloc bloc) {
    bloc.updateTaskFromTaskData();
    Navigator.pop(context);
  }

  Widget _dateTimePicker(
      TaskData data, bool isDisable, TaskOperationBloc bloc) {
    return DateTimeField(
      decoration: InputDecoration(
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        contentPadding: EdgeInsets.all(16.0),
        hintStyle: SharedStyles.mediumText,
      ),
      onChanged: (currentDate) {
        bloc.updateDate(currentDate);
      },
      onEditingComplete: () => bloc.updateButton(),
      initialValue: data.date ?? DataTimeOperation.getOnlyDaysDateTimeFromNow(),
      format: AppConfig.format,
      onShowPicker: (context, currentValue) async {
        final datePicker = await showDatePicker(
          context: context,
          firstDate: DateTime(1900),
          initialDate: currentValue ??
              data.date ??
              DataTimeOperation.getOnlyDaysDateTimeFromNow(),
          lastDate: DateTime(2100),
        );
        if (datePicker != null) {
          final time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(
                currentValue ?? DataTimeOperation.getOnlyDaysDateTimeFromNow()),
          );
          return DateTimeField.combine(datePicker, time);
        } else
          return currentValue;
      },
    );
  }

  Widget _submitButton(TaskData data, bool isDisable, BuildContext context,
      TaskOperationBloc bloc) {
    return RaisedButton(
      color: SharedStyles.themeColors['primaryColor'],
      textColor: SharedStyles.themeColors['thirdColor'],
      shape: RoundedRectangleBorder(
          borderRadius: SharedStyles.borderRadiusForInput,
          side: BorderSide(color: Colors.white)),
      onPressed: isDisable ? null : () => _submitTask(context, bloc),
      child: Text(
        StringLiterals.ADD_TASK_VIEW_BUTTON_SAVE_TEXT,
        style: SharedStyles.mediumText,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    TaskOperationBloc bloc = Provider.of<TaskOperationBloc>(context);
    return Scaffold(
      appBar: SharedViewWidget.appBar(
        title: StringLiterals.TASK_DETAILS_VIEW_TITLE,
      ),
      body: Container(
        margin: EdgeInsets.all(8.0),
        child: StreamBuilder(
          stream: bloc.taskData,
          builder: (BuildContext context, AsyncSnapshot<TaskData> snapshot) {
            if (snapshot.hasData) {
              return StreamBuilder(
                stream: bloc.isDisable,
                initialData: false,
                builder: (BuildContext context, AsyncSnapshot<bool> isDisable) {
                  if (snapshot.hasData) {
                    return Column(
                      children: <Widget>[
                        TextFormField(
                            style: SharedStyles.mediumText,
                            textInputAction: TextInputAction.done,
                            onFieldSubmitted: isDisable.data
                                ? null
                                : (str) => _submitTask(context, bloc),
                            initialValue: snapshot.data.title,
                            autofocus: true,
                            autocorrect: true,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      SharedStyles.borderRadiusForInput),
                              hintText:
                                  StringLiterals.ADD_TASK_VIEW_TITLE_HINT_TEXT,
                              contentPadding: EdgeInsets.all(16.0),
                              hintStyle: SharedStyles.mediumText,
                            ),
                            onChanged: bloc.updateTitle),
                        SizedBox(height: 10),
                        _dateTimePicker(snapshot.data, isDisable.data, bloc),
                        _submitButton(
                            snapshot.data, isDisable.data, context, bloc),
                      ],
                    );
                  } else
                    return SizedBox.shrink();
                },
              );
            } else
              return SizedBox.shrink();
          },
        ),
      ),
    );
  }
}
