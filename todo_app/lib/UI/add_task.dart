import 'package:flutter/material.dart';
import 'package:todo_app/models/task.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:uuid/uuid.dart';
import 'package:todo_app/blocs/to_do_list_bloc.dart';
import 'package:todo_app/blocs/bloc_provider.dart';
import 'package:todo_app/assets/enum_options.dart';
import 'package:todo_app/services/date_time_operation.dart';
import 'package:todo_app/assets/string_literals.dart';
import 'package:todo_app/assets/shared_styles.dart';
import 'package:todo_app/app_config.dart';
import 'package:todo_app/assets/shared_view_widgets.dart';

class AddTask extends StatefulWidget {
  @override
  _AddTask createState() => _AddTask();
}

class _AddTask extends State<AddTask> {
  TextEditingController _controller = TextEditingController();
  bool _isButtonDisable = true;
  DateTime date = DataTimeOperation.getOnlyDaysDateTimeFromNow();
  ToDoListBloc bloc;

  void _setButton(String str) {
    setState(() {
      _isButtonDisable = str.length > 0 && date != null ? false : true;
    });
  }

  void _submitTask(String str) {
    if (_controller.text.length > 0) {
      bloc.operation.add({
        OperationType.adding:
            TaskData(title: _controller.text, date: date, id: Uuid().v1())
      });
    }
    Navigator.pop(context);
  }

  Widget _dateTimePicker() {
    return DateTimeField(
      decoration: InputDecoration(
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        contentPadding: EdgeInsets.all(16.0),
        hintStyle: SharedStyles.mediumText,
      ),
      onChanged: (currentDate) {
        date = currentDate;
        _setButton(_controller.text);
      },
      onEditingComplete: () => _setButton,
      initialValue: date,
      format: AppConfig.format,
      onShowPicker: (context, currentValue) async {
        final date = await showDatePicker(
          context: context,
          firstDate: DateTime(1900),
          initialDate: currentValue ?? DateTime.now(),
          lastDate: DateTime(2100),
        );
        if (date != null) {
          final time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(
                currentValue ?? DataTimeOperation.getOnlyDaysDateTimeFromNow()),
          );
          return DateTimeField.combine(date, time);
        } else
          return currentValue;
      },
    );
  }

  Widget _submitButton() {
    return RaisedButton(
      color: SharedStyles.themeColors['primaryColor'],
      textColor: SharedStyles.themeColors['thirdColor'],
      shape: RoundedRectangleBorder(
          borderRadius: SharedStyles.borderRadiusForInput,
          side: BorderSide(color: Colors.white)),
      onPressed: _isButtonDisable ? null : () => _submitTask(_controller.text),
      child: Text(
        StringLiterals.ADD_TASK_VIEW_BUTTON_SAVE_TEXT,
        style: SharedStyles.mediumText,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<ToDoListBloc>(context);
    return Scaffold(
      appBar:
          SharedViewWidget.appBar(title: StringLiterals.ADD_TASK_VIEW_TITLE),
      body: Container(
        margin: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            TextField(
              style: SharedStyles.mediumText,
              textInputAction: TextInputAction.done,
              onSubmitted: _isButtonDisable ? null : _submitTask,
              controller: _controller,
              autofocus: true,
              autocorrect: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: SharedStyles.borderRadiusForInput),
                hintText: StringLiterals.ADD_TASK_VIEW_TITLE_HINT_TEXT,
                contentPadding: EdgeInsets.all(16.0),
                hintStyle: SharedStyles.mediumText,
              ),
              onChanged: _setButton,
            ),
            SizedBox(height: 10),
            _dateTimePicker(),
            _submitButton(),
          ],
        ),
      ),
    );
  }
}
