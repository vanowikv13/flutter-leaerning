import 'package:flutter/material.dart';
import 'package:todo_app/UI/to_do_list.dart';
import 'package:todo_app/assets/enum_options.dart';
import 'package:todo_app/assets/shared_styles.dart';
import 'package:todo_app/assets/shared_view_widgets.dart';
import 'package:todo_app/assets/string_literals.dart';
import 'package:todo_app/blocs/auth_bloc.dart';
import 'package:todo_app/blocs/bloc_provider.dart';
import 'package:todo_app/blocs/to_do_list_bloc.dart';

class Auth extends StatefulWidget {
  Auth({Key key}) : super(key: key);

  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  final _formKey = GlobalKey<FormState>();
  AuthBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = Provider.of<AuthBloc>(context);
    _bloc.message.listen((message) {
      if (message.containsKey(AuthMessage.uid))
        _navigateToListPage(context, message[AuthMessage.uid]);
    });
  }

  String _getTextOption(isSignUp) {
    return isSignUp
        ? StringLiterals.SIGN_UP_TITLE
        : StringLiterals.SIGN_IN_TITLE;
  }

  String _validateEmail(String email) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\"r.+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(email)) return StringLiterals.VALIDATOR_BAD_EMAIL;
    return null;
  }

  String _validatePassword(String pass) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    if (!regExp.hasMatch(pass)) return StringLiterals.VALIDATOR_BAD_PASSWORD;
    return null;
  }

  void _navigateToListPage(context, uid) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Provider(
            child: MaterialApp(
              home: ListPage(),
              
            ),
            bloc: ToDoListBloc(uid),
          ),
        ),
      );
  }

  Widget _showInput({bloc, isEmail = false}) {
    return TextFormField(
        onSaved: isEmail ? bloc.setLogin : bloc.setPassword,
        maxLines: 1,
        autofocus: true,
        style: SharedStyles.textAuthInput,
        obscureText: !isEmail,
        keyboardType: isEmail
            ? TextInputType.emailAddress
            : TextInputType.visiblePassword,
        decoration: InputDecoration(
            labelText: isEmail
                ? StringLiterals.EMAIL_LABEL
                : StringLiterals.PASSWORD_LABEL,
            labelStyle: SharedStyles.authInputLabelTextStyle,
            icon: Icon(
              Icons.mail,
              color: SharedStyles.authInputIconColor,
            ),
            border: SharedStyles.authInputBorder,
            focusedBorder: SharedStyles.authInputBorder),
        validator: (value) {
          if (value.isEmpty) {
            return isEmail
                ? StringLiterals.VALIDATOR_EMPTY_EMAIL_MESSAGE
                : StringLiterals.VALIDATOR_EMPTY_PASSWORD_MESSAGE;
          }
          return isEmail ? _validateEmail(value) : _validatePassword(value);
        });
  }

  Widget _showButton(bloc, isSignUp) {
    return RaisedButton(
      child: Text(isSignUp
          ? StringLiterals.SIGN_UP_TITLE
          : StringLiterals.SIGN_IN_TITLE),
      onPressed: () {
        final form = _formKey.currentState;
        form.save();
        if (form.validate()) {
          if (isSignUp) {
            bloc.signUp();
          } else {
            bloc.signIn();
          }
        }
      },
    );
  }

  Widget _messageStreamBuilder(bloc) {
    return StreamBuilder(
        stream: bloc.message,
        builder: (BuildContext context, AsyncSnapshot user) {
          if (user.hasData) {
            if (user.data.containsKey(AuthMessage.error)) {
              return Container(
                margin: EdgeInsets.only(
                    top: 5.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: Text(
                    user.data[AuthMessage.error] ??
                        StringLiterals
                            .ERROR_MESSAGE_IN_LOGIN_WHEN_FIREBASE_DOES_NOT_WORK,
                    style: SharedStyles.errorMessage),
              );
            }
          }
          return Container();
        });
  }

  @override
  Widget build(BuildContext context) {
    var isSignUp = false;
    return StreamBuilder(
      stream: _bloc.authOperation,
      builder: (BuildContext context, AsyncSnapshot<AuthOption> snapshot) {
        if (snapshot.hasData) {
          isSignUp = snapshot.data == AuthOption.signUp;
          return Scaffold(
              appBar: SharedViewWidget.appBar(
                title: _getTextOption(isSignUp),
                actions: <Widget>[
                  FlatButton.icon(
                    icon: Icon(Icons.person),
                    label: Text(_getTextOption(!isSignUp)),
                    onPressed: () => _bloc.changeAuthOperation(),
                  ),
                ],
              ),
              body: Container(
                padding: EdgeInsets.all(20.0),
                child: Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      _showInput(bloc: _bloc, isEmail: true),
                      SizedBox(height: 20.0),
                      _showInput(bloc: _bloc),
                      SizedBox(height: 20.0),
                      _messageStreamBuilder(_bloc),
                      _showButton(_bloc, isSignUp),
                    ])),
              ));
        } else {
          return Scaffold(
            appBar: SharedViewWidget.appBar(title: ""),
            body: SizedBox.shrink(),
          );
        }
      },
    );
  }
}
