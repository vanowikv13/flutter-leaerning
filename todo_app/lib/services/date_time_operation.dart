import 'package:todo_app/app_config.dart';

class DataTimeOperation {
  static int getDateTimeAsTimestamp(DateTime date) {
    return date.millisecondsSinceEpoch;
  }

  static DateTime getDateTimeFromUnixEpoch(int date) {
    return DateTime.fromMillisecondsSinceEpoch(date);
  }

  static DateTime getOnlyDaysDateTimeFromNow() {
    DateTime date = DateTime.now();
    return DateTime(date.year, date.month, date.day);
  }

  static String getFormatedData(DateTime date) {
    return AppConfig.format.format(date);
  }
}