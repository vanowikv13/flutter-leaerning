import 'package:todo_app/app_config.dart';
import 'package:todo_app/models/task.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {
  String _uid;
  final _databaseReference = Firestore.instance;
  var _tasks;

  DatabaseService({uid}) {
    this._uid = uid;
  }

  Future<DocumentSnapshot> _getDocuments() async {
    return await _getDocumentPath().get();
  }

  DocumentReference _getDocumentPath() {
    return _databaseReference.collection(AppConfig.taskDatabaseName).document(_uid);
  }

  List<TaskData> _convertMapToListTaskData() {
    if (_tasks.length == 0) return [];
    List<TaskData> list = [];
    _tasks.forEach((k, v) {
      list.add(TaskData.setFromMap(k, v));
    });
    return list;
  }

  Future<List<TaskData>> getTaskList() async {
    if (_tasks == null) {
      await _getDocuments().then((value) {
        if (value.exists)
          _tasks = value.data;
        else
          _tasks = {};
      }).catchError((error) {
        print(error);
      });
    }
    return _convertMapToListTaskData();
  }

  void deleteTask(String id) async {
    try {
      _getDocumentPath()
          .updateData({id: FieldValue.delete()}).whenComplete(() {
        print('Field Deleted with id: $id');
      });
    } catch (e) {
      print(e);
    }
  }

  void addTask(TaskData task) async {
    try {
      _getDocumentPath().setData(task.getMapFromTaskData(), merge: true);
    } catch (e) {
      print(e);
    }
  }

  void updateData(TaskData data) {
    try {
      _getDocumentPath().updateData(data.getMapFromTaskData());
    } catch (e) {
      print(e.toString());
    }
  }
}
