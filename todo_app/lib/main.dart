import 'package:flutter/material.dart';
import 'package:todo_app/UI/auth.dart';
import 'package:todo_app/blocs/auth_bloc.dart';
import 'package:todo_app/blocs/bloc_provider.dart';
import 'package:todo_app/assets/string_literals.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      bloc: AuthBloc(),
      child: MaterialApp(
        title: StringLiterals.MAIN_TITLE,
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        home: Auth()
      ),
    );
  }
}
