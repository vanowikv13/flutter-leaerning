import 'package:todo_app/services/date_time_operation.dart';

class TaskData {
  String id;
  String title;
  bool isDone;
  DateTime date;

  TaskData({this.title, this.id, this.isDone = false, this.date});

  TaskData.setFromMap(String id, task) {
    this.id = id;
    title = task["title"];
    isDone = task["isDone"];
    date = DataTimeOperation.getDateTimeFromUnixEpoch(task["date"]);
  }

  Map<String, Map<String, dynamic>> getMapFromTaskData() {
    return {
      id : {
        'title': title,
        'date': DataTimeOperation.getDateTimeAsTimestamp(date),
        'isDone': isDone,
      }
    };
  }
}